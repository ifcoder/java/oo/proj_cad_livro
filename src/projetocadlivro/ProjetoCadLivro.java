package projetocadlivro;

import classes.Livro;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author jose
 */
public class ProjetoCadLivro {

    public static int imprimeMenu() {
        Scanner leitor = new Scanner(System.in);

        System.out.println("------------------------------");
        System.out.println("1 - Inserir livro");
        System.out.println("2 - Remover livro");
        System.out.println("3 - Pesquisar livro (titulo)");
        System.out.println("4 - Pesquisar livro (autor)");
        System.out.println("0 - Sair");
        System.out.println("-----------------------------");
        System.out.println("Escolha uma opção:");

        return leitor.nextInt();
    }

    public static void main(String[] args) {
        List<Livro> listaLivros;
        listaLivros = new ArrayList<Livro>();
        Scanner leitor = new Scanner(System.in);
        int opcao = 0;
        
        do {
            opcao = imprimeMenu();
            if (opcao == 1) { //insercao
                Livro l1 = new Livro();
                l1.preencher();
                listaLivros.add(l1);
            } else if (opcao == 2) {
                System.out.println("Informe o titulo:");
                String title = leitor.nextLine();

                for (int i = 0; i <= listaLivros.size() - 1; i++) {
                    Livro li = listaLivros.get(i);
                    if (title.equals(li.getTitulo())) {
                        System.out.println("Livro encontrado");
                        listaLivros.remove(li);
                    }
                }
                //remocao
            } else if (opcao == 3) {
                System.out.println("Informe o titulo:");
                String title = leitor.nextLine();

                for (int i = 0; i <= listaLivros.size() - 1; i++) {
                    Livro li = listaLivros.get(i);
                    if (title.equals(li.getTitulo())) {
                        System.out.println("Livro encontrado");
                        li.imprimir();
                    }
                }
                //pesquisa titulo
            } else if (opcao == 4) {
                //pesquisa autor

                System.out.println("Informe o autor:");
                String autor = leitor.nextLine();

                for (int i = 0; i <= listaLivros.size() - 1; i++) {
                    Livro li = listaLivros.get(i);
                    if (autor.equals(li.getAutor())) {
                        System.out.println("Autor encontrado");
                        li.imprimir();
                    }

                }
            }
        } while (opcao != 0);

    }

}
