
package classes;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Livro {
    private String titulo;
    private String autor;
    private int pag;
    private double preco;
    
    public Livro(){
        this.titulo = "";
        this.autor = "";
        this.pag = 0;
        this.preco = 0.0;
    }
    
    public void preencher(){
        Scanner leitor = new Scanner(System.in);
        System.out.println("--------- Preenchendo os dados de livro -------- ");
        
        System.out.println("Informe o titulo:");
        this.titulo = leitor.nextLine();
        System.out.println("Informe o autor:");
        this.autor = leitor.nextLine();
        System.out.println("Informe o numero de paginas:");
        this.pag = leitor.nextInt();
        System.out.println("Informe o preco:");
        this.preco = leitor.nextDouble();
    }
    
    public void imprimir(){
        System.out.println("------------------------");
        System.out.println("Titulo:"+ this.titulo);
        System.out.println("Autor:"+ this.autor);
        System.out.println("Paginas:"+ this.pag);
        System.out.println("Preço:" + this.preco);  
        System.out.println("------------------------");
    }
    
    public void copiar(Livro outro){
        this.titulo = outro.getTitulo();
        this.autor = outro.getAutor();
        this.pag = outro.getPag();
        this.preco = outro.getPreco();
    }
    
    
    public String getTitulo(){
        return this.titulo;
    }
    
    public void setTitulo(String titulo){
        this.titulo = titulo;
    }

    
    public String getAutor() {
        return autor;
    }


    public void setAutor(String autor) {
        this.autor = autor;
    }

 
    public int getPag() {
        return pag;
    }

 
    public void setPag(int pag) {
        this.pag = pag;
    }

  
    public double getPreco() {
        return preco;
    }

 
    public void setPreco(double preco) {
        this.preco = preco;
    }
    
    
}
