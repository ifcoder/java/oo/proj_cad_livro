# Projeto Cadastro de Livro

### Exercício 11 - seção anterior

Agora que já temos a classe **Livro**📕 prontinha para usar, faça um **Cadastro de Livro**, onde será possível adicionar um livro, remover um livro e pesquisar um livro pelo titulo e pelo autor.

![Saida Cadastro Livros](https://drive.google.com/file/d/1Z4UcAFab4IyW15pwLgaxTX4X4QLubNx4/view?usp=sharing)
                       

- A partir deste exercício, podemos fazer sistemas cada vez mais legais e mais próximos da realidade.

- [2.2. Classes sem relacionamentos - Cadastro de Livro](https://summer-pocket-6a4.notion.site/2-2-Classes-sem-relacionamentos-Cadastro-de-Livro-04185e2b45b84009b480d7dff2dac6a8) 

  
